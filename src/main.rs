extern crate ncurses;
extern crate rgui;
extern crate td_rlua;
extern crate td_clua;

use td_rlua::{Lua, LuaPush, LuaTable};
use rgui::{ScreenBuilder};

use std::ffi::CString;

mod lua;
use lua::{LuaWindow, Event};

struct App {
    lua_s: Lua,
    screen: LuaWindow,
}

impl App {
    fn new() -> App {
        // Let us build our screen with a specific locale detected automatically:
        let mut screen = LuaWindow::new_from_window(
            ScreenBuilder::new()
            // .raw()          // We want to catch everything
            .keypad(true)   // We get F_Keys and numpad keys
            .no_echo()      // No need to print what the user is typing
            .build()        // Let us get the created Window struct.
            );

        // Initialising the Lua state:
        let mut lua_s = Lua::new();
        // Opening all libs for now:
        lua_s.openlibs();

        App::_export_app(&mut lua_s, &mut screen);

        // Loading the main game script:
        // let _ : Option<()> = lua_s.exec_string("require 'scripts.key'".to_string());
        let _ : Option<()> = lua_s.exec_string("dofile('scripts/key.lua')".to_string());

        App {
            lua_s: lua_s,
            screen: screen,
        }
    }

    fn _export_app(lua_s: &mut Lua, screen: &mut LuaWindow) {
        unsafe {
            td_rlua::lua_newtable(lua_s.state());

            "quit".push_to_lua(lua_s.state());
            false.push_to_lua(lua_s.state());
            td_rlua::lua_settable(lua_s.state(), -3);

            "screen".push_to_lua(lua_s.state());
            screen.push_to_lua(lua_s.state());
            td_rlua::lua_settable(lua_s.state(), -3);

            td_clua::lua_setglobal(lua_s.state(), CString::new("app").unwrap().as_ptr());
        }
    }

    fn mainloop(&mut self) {
        self.screen.win.printw("Welcome\n");
        loop {
            let mut ev = Event::new(self.screen.win.get_input());
            self.screen.win.printw(&format!("{}\n", self.lua_s.exec_func1("event", & mut ev)));

            if self.quit() {
                println!("Quitting");
                break;
            }
        }
    }

    fn quit(&mut self) -> bool {
        let mut table: LuaTable = self.lua_s.query("app").unwrap();

        let res: Option<bool> = table.query("quit");
        res.unwrap()
    }
}

fn main() {
    let mut app: App = App::new();

    app.mainloop();
}
