extern crate td_rlua;
use td_rlua::{LuaPush, lua_State};

use rgui::KeyEvent;

#[derive(Debug)]
pub struct Event {
    key: Option<u32>,
    ch: Option<char>,
}

impl Event {
    pub fn new(ev: KeyEvent) -> Event {
        match ev {
            KeyEvent::Key(a, b) => {
                Event {key:Some(a), ch:b}
            },

            KeyEvent::Missing => {
                Event {key:None, ch:None}
            },
        }
    }
}

impl<'a> LuaPush for &'a mut Event {
    fn push_to_lua(self, lua: *mut lua_State) -> i32 {
        unsafe {
            td_rlua::lua_newtable(lua);

            "key".push_to_lua(lua);
            match self.key {
                None => {
                    ().push_to_lua(lua);
                },
                Some(a) => {
                    a.push_to_lua(lua);
                },
            }
            td_rlua::lua_settable(lua, -3);

            "char".push_to_lua(lua);
            match self.ch {
                None => {
                    ().push_to_lua(lua);
                },
                Some(a) => {
                    a.to_string().push_to_lua(lua);
                },
            }
            td_rlua::lua_settable(lua, -3);

            1
        }
    }
}
