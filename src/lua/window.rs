extern crate td_rlua;
use td_rlua::{LuaPush, LuaRead, userdata, lua_State};

use rgui::Window;

pub struct LuaWindow {
    pub win: Window,
}

impl LuaWindow {
    pub fn new(y: i32, x: i32, height: i32, width: i32) -> LuaWindow {
        LuaWindow {
            win: Window::new(y, x, height, width)
        }
    }

    pub fn new_from_window(w: Window) -> LuaWindow {
        LuaWindow {win: w}
    }
}

impl<'a> LuaPush for &'a mut LuaWindow {
    fn push_to_lua(self, lua: *mut lua_State) -> i32 {
        userdata::push_userdata(self, lua, |_|{})
    }
}

impl<'a> LuaRead for &'a mut LuaWindow {
    fn lua_read_with_pop(lua: *mut lua_State, index: i32, _pop: i32) -> Option<&'a mut LuaWindow> {
        userdata::read_userdata(lua, index)
    }
}

