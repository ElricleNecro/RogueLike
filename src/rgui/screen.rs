extern crate ncurses;

use ncurses as nc;

use super::window::Window;

use std::env;

pub struct ScreenBuilder {
    screen: Window
}

impl ScreenBuilder {
    pub fn new() -> ScreenBuilder {
        match env::var("LANG") {
            Ok(val) => {
                nc::setlocale(nc::LcCategory::all, val.as_str());
            },
            Err(e) => println!("No locale found ({}), using default.", e),
        }

        nc::initscr();

        ScreenBuilder {
            screen: Window::screen(),
        }
    }

    pub fn new_with_locale(s: &str) -> ScreenBuilder {
        let locale_conf = nc::LcCategory::all;
        nc::setlocale(locale_conf, s);

        nc::initscr();

        ScreenBuilder {
            screen: Window::screen(),
        }
    }

    pub fn raw(self) -> ScreenBuilder {
        nc::raw();

        self
    }

    pub fn keypad(self, b: bool) -> ScreenBuilder {
        self.screen.keypad(b);

        self
    }

    pub fn no_echo(self) -> ScreenBuilder {
        nc::noecho();

        self
    }

    pub fn build(self) -> Window {
        self.screen
    }
}
