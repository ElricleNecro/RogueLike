extern crate ncurses;

pub use ncurses::constants as c;

pub use ncurses::{
    A_NORMAL,
    A_ATTRIBUTES,
    A_CHARTEXT,
    A_COLOR,
    A_STANDOUT,
    A_UNDERLINE,
    A_REVERSE,
    A_BLINK,
    A_DIM,
    A_BOLD,
    A_ALTCHARSET,
    A_INVIS,
    A_PROTECT,
    A_HORIZONTAL,
    A_LEFT,
    A_LOW,
    A_RIGHT,
    A_TOP,
    A_VERTICAL,
    COLOR_PAIR,
    COLOR_BLACK,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_YELLOW,
    COLOR_BLUE,
    COLOR_MAGENTA,
    COLOR_CYAN,
    COLOR_WHITE,
};


