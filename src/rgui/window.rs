extern crate ncurses;

use ncurses as nc;

use std::char;
use std::option::Option;

pub struct Window {
    win: nc::WINDOW,

    y: i32,
    x: i32,

    h: i32,
    w: i32,
}

pub enum KeyEvent {
    Key(u32, Option<char>),
    Missing
}

impl Window {
    pub fn new(y: i32, x: i32, height: i32, width: i32) -> Window {
        Window {
            win: nc::newwin(height, width, y, x),
            y: y,
            x: x,
            h: height,
            w: width,
        }
    }

    pub fn screen() -> Window {
        Window {
            win: nc::stdscr(),
            h:   nc::LINES(),
            w:   nc::COLS(),
            y:   0,
            x:   0,
        }
    }

    pub fn keypad(&self, b: bool) -> i32 {
        nc::keypad(self.win, b)
    }

    pub fn is_keypad(&self) -> bool {
        nc::is_keypad(self.win)
    }

    pub fn get_wh(&self) -> (i32, i32) {
        (self.w, self.h)
    }

    pub fn refresh(&self) {
        nc::wrefresh(self.win);
    }

    pub fn getch(&self) -> i32 {
        nc::wgetch(self.win)
    }

    pub fn mvgetch(&self, y: i32, x: i32) -> i32 {
        nc::mvwgetch(self.win, y, x)
    }

    pub fn get_wch(&self) -> Option<nc::WchResult> {
        nc::wget_wch(self.win)
    }

    pub fn get_input(&self) -> KeyEvent {
        match self.get_wch() {
            Some(nc::WchResult::KeyCode(c)) => {
                KeyEvent::Key(c as u32, None)
            },

            Some(nc::WchResult::Char(c)) => {
                KeyEvent::Key(c, char::from_u32(c))
            },

            None => {
                KeyEvent::Missing
            },
        }
    }

    pub fn mvget_wch(&self, y: i32, x: i32) -> Option<nc::WchResult> {
        nc::mvwget_wch(self.win, y, x)
    }

    pub fn printw(&self, s: &str) -> i32 {
        nc::wprintw(self.win, s)
    }

    pub fn mvprintw(&self, y: i32, x: i32, s: &str) -> i32 {
        nc::mvwprintw(self.win, y, x, s)
    }

    pub fn attron(&self, attrs: u32) -> i32 {
        nc::wattron(self.win, attrs)
    }

    pub fn attroff(&self, attrs: u32) -> i32 {
        nc::wattroff(self.win, attrs)
    }

    pub fn boxed(&self) -> i32 {
        nc::box_(self.win, 0, 0)
    }

    pub fn delwin(&self) -> i32 {
        if self.win != nc::stdscr() {
            nc::delwin(self.win)
        } else {
            nc::endwin()
        }
    }
}

impl Drop for Window {
    fn drop(&mut self) {
        self.delwin();
    }
}
